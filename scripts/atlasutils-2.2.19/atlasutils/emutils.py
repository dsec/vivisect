#!/usr/bin/env python
import sys, struct
from ihexparser import *
import struct
import envi
import envi.archs.t80515 as e_arch
from envi.archs.t80515.regs import *
import envi.memory as e_m
import atlasutils.smartprint as sp

if len(sys.argv) != 2:
    print "Syntax:  %s somefilename.hex"%sys.argv[0]
    print "Found:\n"+repr(sys.argv)

inf = sys.argv[-1]  # take the last one
INF = file(inf)
print "Reading from %s"%inf

IRAM_OFFSET = 0
FLASH_OFFSET = 0

def prtMemStuff(emu):
    global IRAM_OFFSET, IRAM_SIZE, FLASH_OFFSET

    pc = emu.getProgramCounter()
    SP = emu.getStackCounter()
    mem = emu.readMemory(pc+FLASH_OFFSET,10)
    print "\tPC memory: FLASH:0x%0.4x\t%s" % (pc, sp.hexText(mem))
    mem = emu.readMemory(SP+IRAM_OFFSET,20)
    print "\tSP memory: IRAM: 0x%0.4x\t%s" % (SP, sp.hexText(mem))
    mem = emu.readMemory(IRAM_OFFSET+32, IRAM_SIZE-32)
    print "IRAM: "#+sp.hexText(mem)
    print XW(emu, IRAM_OFFSET, (IRAM_SIZE)/4)

def XW(tracer, address, length = 32, dwperline = 8):
    output = []
    for i in range(length):
        if (i % dwperline == 0):
            output.append( "%.08x:\t "%(address+(i*4)))
        bs = tracer.readMemory(address+(i*4),4)
        for x in range(4):
            output.append("%.02x"%(ord(bs[x])))
        if ((i+1) % dwperline == 0):
            output.append("\n")
        else:
            output.append("  ")
    return "".join(output)



def showPriRegisters(emu, all=True):
    print "\nRegisters:"
    if all:
        for i in range(0,emu._arch_dis.getRegisterCount()):
            if (i%5 == 0):
                sys.stdout.write("\n")#%4x"%i)
            sys.stdout.write("%15s: %4x" % (reg_table[i][0], emu.getRegister(i)))
    else:
        # Just output the primary registers
        for i in range(0,8,2):
            if (i%5 == 0):
                sys.stdout.write("\n")#%4x"%i)
            sys.stdout.write("%15s: %4x" % (reg_table[i][0], emu.getRegister(i)))
    # Line feed
    print

def showFlags(self):
    """
    Show the contents of the Status Register
    """
    print "\tStatus Flags: \tRegister: %s\n\tC: %s (%s)\t\tO: %s (%s)\n\tAC : %s (%s)\t\tRegBank: %d%d " % (bin(self.getRegister(REG_PSW)), self.getFlag(PSW_C), int(self.getFlag(PSW_C)), self.getFlag(PSW_OV), int(self.getFlag(PSW_OV)), self.getFlag(PSW_AC), int(self.getFlag(PSW_AC)), self.getFlag(PSW_RS0), self.getFlag(PSW_RS1))

    # Line feed
    print

def runStep(emu, maxstep=1000000):
    global op
    #emu.setProgramCounter(pc)
    #showPriRegisters(emu, True)
    #showFlags(emu)
    for i in range(maxstep):
        showPriRegisters(emu, True)
        showFlags(emu)
        prtMemStuff(emu)
        pc=emu.getProgramCounter()
        op=emu.makeOpcode(pc)
        opbytes = emu.readMemory(pc,len(op))
        #print XW(
        print "Step: %s" % i
        print "%.4x\t%20s\t%s"%(pc,sp.hexText(opbytes),op)
        print "---------"
        quit = raw_input(["q-enter to exit, enter to continue"])
        if quit == "q":
            break
        emu.stepi()
        #prtInst(emu)
        #showPriRegisters(emu,True)
        #showFlags(emu)

def runUntil(emu, eip=0, mnem="int", maxstep=1000000):
    global op
    for i in range(maxstep):
        pc=emu.getProgramCounter()
        op=emu.makeOpcode(pc)
        opbytes = emu.readMemory(pc,len(op))
        if pc == eip or op.mnem == mnem:
            break
        emu.stepi()
    runStep(emu)

def selectTest():

    print "The following tests implement T80515 disassembly or emulation.\n\n"

    print "Select Test: \n[0] Disassemble Only \n[1] Emulation \n",
    dotest = raw_input()
    if dotest.isdigit():
        return int(dotest)
    else:
        print "Unknown test selected"
        sys.exit()


# Parse the file with ihexparser
memory = ihexbin(INF.read())

doTest = selectTest()
e=None
if doTest:
    # Do Emulation
    staddr = memory.startaddress
    print "Start Addr: %x" % staddr
    for addr,typ,data in memory.sections:
        if typ == 0:
            break
    output = data
    #output2 = "%x" % 0x00 * 0x4000
    output2 =  '\x00' * staddr
    output3 = "\x00" * 0xffff


    e=e_arch.T80515Emulator()
    e.memobj.addMemoryMap(addr,0777,"Memory Map?",output2 + output)# + output3)

    FLASH_BASE, FLASH_SIZE, FLASH_OFFSET, FLASH_NAME = e.segments[e.SEG_FLASH]
    IRAM_BASE, IRAM_SIZE, IRAM_OFFSET, IRAM_NAME = e.segments[e.SEG_IRAM]

    maxSteps = 200

    runStep(e)

else:
    # Do Disassembly

    # Send data to msp430.py
    # and let it do processing
    #process_data(output)
    #process_data2(output)
    output = memory.bin_gen()
    a = e_arch.T80515Module()
    offset = 0
    #for offset in range(0, len(output) - 2):
    while offset < len(output):
        inOp = a.makeOpcode(output[offset:offset+6], va=offset)
        bytes = output[offset:offset+len(inOp)]
        print "%.4x:\t%32s\t\t(len:%x)  (bytes: '%s')"%(inOp.va,repr(inOp), len(inOp), sp.hexText(bytes))
        offset += len(inOp)

