#!/usr/bin/python
from socket import *
from sys import *
from hacklib import *
from select import *

if (len(argv) < 2):
	syntax()
	exit(1)

host = argv[1]
port = 6969


TIMEOUT = 10

####### Used for fuzzing
#for (my $i = 1038; ; $i++){
#######
# i = 1063 - length($string);

s = socket.socket(AF_INET, SOCK_STREAM, getprotobyname('tcp'))
s.connect((host,port))



### all sends can follow this pattern
s.sendall("FUZZER in need of assistance!\n");    # sendall is send and flush wrapped into one



### all recvs can follow this pattern
#x,y,z = select([s],[],[], TIMEOUT)
#if x:
#	s.recv(65768)
#else:
#	print >>stderr, ('ERROR: Timed out waiting for response from server.')


def go(s):
    print >>sys.stderr,("\n[+] Entering Interactive Shell...")
    while True:
        x,y,z = select.select([sys.stdin, s],[],[],.1)
        if s in x:
            ch = s.recv(1000)
            if len(ch) == 0: break
            print(ch)
        if sys.stdin in x:
            ch = os.read(sys.stdin.fileno(), 1000)
            #print(repr(ch))
            s.sendall(ch)

go()

s.close()

