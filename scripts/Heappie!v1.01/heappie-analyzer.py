# Copyright (c) 2012, CORE SECURITY TECHNOLOGIES
# All rights reserved.
#!/bin/python

import vtrace
import sys
import os
import optparse


# ##############################################
# vtrace Debugger object
# ##############################################

class Debugger ( vtrace.Notifier ):
    def notify ( self , event , trace ):
        # if loading a DLL
#        if ( event == vtrace.NOTIFY_LOAD_LIBRARY ):
#            pass
        
        # When attached
        if ( event == vtrace.NOTIFY_ATTACH ):
            print "[+] Attached"
        
        # If a breakpoint was hit
        elif ( event == vtrace.NOTIFY_BREAK ):
            print "[ ] Breakpoint hit" #debug
            # Stop execution
            #trace.setMode ( "RunForever" , False )
        
        # If another exception occurs
        elif ( event == vtrace.NOTIFY_SIGNAL ):
            print "[+] Exception detected"
            #keep running
            #trace.setMode ( "RunForever" , True )

# ##############################################
# Memorydump object
# ##############################################

class memorydump:
    def __init__(self, filename, baseaddr):
        self.string = file(filename, "rb").read()
        self.baseaddr = baseaddr

    def searchMemory(self, pattern):
        positions = []
        end = len(self.string)
        pos = 0
        while pos < end:
            pos = self.string.find(pattern, pos)
            if pos == -1:
                break
            positions.append(self.baseaddr + pos)
            pos += len(pattern)
        return positions




def search(trace, pattern):
    size = len(pattern)
    prev_addr = 0
    blocks = {}
    slicex = 0

    for addr in trace.searchMemory(pattern):
        
        if prev_addr + len(pattern) == addr: #if  it's a consecutive match
            size += 1
            l = blocks.get(addr-(size*len(pattern)), 0)
            blocks[addr-(size*len(pattern))] = l+len(pattern)
            #blocks.setdefault(addr-(size*len(pattern))) += len(pattern)
        else:
            size = 0
        
        prev_addr = addr

        #data = trace.readMemory(addr, size)
        #print "Pattern found at: 0x%.8x" % addr # (DEBUG)
    
    return blocks


def main():


    
    # Beautiful banner
    banner = r"""
.---.  .---.     .-''-.     ____    .-------. .-------. .-./`)     .-''-.   
|   |  |_ _|   .'_ _   \  .'  __ `. \  _(`)_ \\  _(`)_ \\ .-.')  .'_ _   \  
|   |  ( ' )  / ( ` )   '/   '  \  \| (_ o._)|| (_ o._)|/ `-' \ / ( ` )   ' 
|   '-(_{;}_). (_ o _)  ||___|  /  ||  (_,_) /|  (_,_) / `-'`"`. (_ o _)  | 
|      (_,_) |  (_,_)___|   _.-`   ||   '-.-' |   '-.-'  .---. |  (_,_)___| 
| _ _--.   | '  \   .---..'   _    ||   |     |   |      |   | '  \   .---. 
|( ' ) |   |  \  `-'    /|  _( )_  ||   |     |   |      |   |  \  `-'    / 
(_{;}_)|   |   \       / \ (_ o _) //   )     /   )      |   |   \       /  
'(_,_) '---'    `'-..-'   '.(_,_).' `---'     `---'      '---'    `'-..-'   

::::::::::::::::::::::..:::::..::::::::::::: Heap Spray Analyzer 1.0b ::::
::::::::::::::::::::::..:::::..:::::::::::::::::::::::::::::: by aLS :::::
                    
    """
    print banner
    #print "\n[+] HeapSpray Analyzer 1.5\n"

    # ##############################################
    # Arguments parsing
    # ##############################################

    parser = optparse.OptionParser("%prog {-p pid | -d rawmemorydump -b baseaddress} {pattern} [options] ")

    parser.add_option("-p", "--pid", dest="pid", default=False, type="int", help="The process id")

    parser.add_option("-d", "--dump", dest="dumpfilename", default="dump.bin", type="string", help="The raw memory dump")
    parser.add_option("-b", "--baseaddr", dest="baseaddr", default=0x0, type=int, help="The base memory dump base address")
    
    parser.add_option("-l", "--log", dest="logfilename", default="log.txt", type="string", help="The process argument")
    parser.add_option("-x", action="store_true", dest="hexpattern", help="Process pattern as hex")
    parser.add_option("-v", action="store_true", dest="verbose", help="Also log to stdout")
    parser.add_option("-e", action="store_true", dest="except_flag", help="Start the analysis after an exception")


    (options, args) = parser.parse_args()



    if len(args) < 1:
        parser.error("Incorrect arguments. Use --help for help")

    pid = options.pid
    pattern = args[0]

    if options.hexpattern:
        pattern = pattern.decode("hex")
        

##########################################################################################
##########################################################################################

    if pid: 
        # ##############################################
        # Attach to process and analyze
        # ##############################################    

        process = vtrace.getTrace ()
        print "[+] Attached to process %d" % pid
    
        try:
            process.registerNotifier ( vtrace.NOTIFY_ALL , Debugger () )
            #process.setMode ( "NonBlocking" , True )        
            #process.setMode ( "RunForever" , True )
            process.attach ( pid )
            
            
            # If we need to wait for an exception...
            if options.except_flag:
                print "[+] Waiting for an exception"
                process.run ()
                

            print "[+] Searching...\n"
            blocks = search(process, pattern)
          

            # Generate log
            filelog = open(options.logfilename, "w")
                    
            for addr, size in sorted(blocks.items()):
                str = "Address 0x%08x: 0x%08x bytes\n" % (addr, size)
                filelog.write(str)
                if options.verbose:
                    print str[:-1]

            # Detach
            print "[ ] Detaching..."
            process.detach ()
            print "[+] Done."
            
        except Exception, e:
            print "Oh My Fuck!", e
            process.detach ()
            sys.exit(2)

    else:
        # ##############################################
        # Analyze raw memory dump
        # ##############################################    
        
        print "[+] Analyzing memory dump %s" % options.dumpfilename
        dump_obj = memorydump(options.dumpfilename, options.baseaddr)
        blocks = search(dump_obj, pattern)

        # Generate log
        filelog = open(options.logfilename, "w")
                
        for addr, size in sorted(blocks.items()):
            str = "Address 0x%08x: 0x%08x bytes\n" % (addr, size)
            filelog.write(str)
            if options.verbose:
                print str[:-1]

        print "[+] Analysis finished"

    


if __name__ == "__main__":
    sys.exit(main())
