{'application':{'type':'Application',
          'name':'Template',
    'backgrounds': [
    {'type':'Background',
          'name':'bgTemplate',
          'title':u'Heappie! v1.0',
          'size':(602, 551),

        'menubar': {'type':'MenuBar',
         'menus': [
             {'type':'Menu',
             'name':'menuFile',
             'label':'&File',
             'items': [
                  {'type':'MenuItem',
                   'name':'menuFileExit',
                   'label':'E&xit',
                   'command':'exit',
                  },
              ]
             },
         ]
     },
         'components': [

{'type':'Image', 
    'name':'Image1', 
    'position':(376, 28), 
    'size':(196, 60), 
    'file':'gui/images/logo1-chico.png', 
    },

{'type':'StaticText', 
    'name':'showConsole', 
    'position':(489, 480), 
    'text':'Show console', 
    },

{'type':'ToggleButton', 
    'name':'btnConsole', 
    'position':(563, 477), 
    'size':(19, 18), 
    'label':'+', 
    },

{'type':'TextArea', 
    'name':'txtConsole', 
    'position':(10, 507), 
    'size':(575, 179), 
    'backgroundColor':(0, 0, 0, 255), 
    'editable':False, 
    'font':{'faceName': u'Courier New', 'family': 'sansSerif', 'size': 8}, 
    'foregroundColor':(0, 255, 0, 255), 
    'text':'.---.  .---.     .-\'\'-.     ____    .-------. .-------. .-./`)     .-\'\'-.   \n|   |  |_ _|   .\'_ _   \\  .\'  __ `. \\  _(`)_ \\\\  _(`)_ \\\\ .-.\')  .\'_ _   \\  \n|   |  ( \' )  / ( ` )   \'/   \'  \\  \\| (_ o._)|| (_ o._)|/ `-\' \\ / ( ` )   \' \n|   \'-(_{;}_). (_ o _)  ||___|  /  ||  (_,_) /|  (_,_) / `-\'`"`. (_ o _)  | \n|      (_,_) |  (_,_)___|   _.-`   ||   \'-.-\' |   \'-.-\'  .---. |  (_,_)___| \n| _ _--.   | \'  \\   .---..\'   _    ||   |     |   |      |   | \'  \\   .---. \n|( \' ) |   |  \\  `-\'    /|  _( )_  ||   |     |   |      |   |  \\  `-\'    / \n(_{;}_)|   |   \\       / \\ (_ o _) //   )     /   )      |   |   \\       /  \n\'(_,_) \'---\'    `\'-..-\'   \'.(_,_).\' `---\'     `---\'      \'---\'    `\'-..-\'   \n\n::::::::::::::::::::::..:::::..:::::::: Heap Spray Analyzer :::::::::::::\n:::::::::::::::::::::::..:::::..:::::::: asacco@coresecurity.com (aLS):::\n', 
    },

{'type':'ComboBox', 
    'name':'SelectAnalysis', 
    'position':(127, 46), 
    'size':(138, -1), 
    'items':[u'Attach to process', u'Import memory dump'], 
    'text':'Select type of analysis', 
    },

{'type':'StaticText', 
    'name':'typeOfAnalysis', 
    'position':(26, 52), 
    'text':'Type of analysis', 
    },

{'type':'Button', 
    'name':'btIntersect', 
    'position':(399, 385), 
    'label':'Add more logs', 
    },

{'type':'StaticText', 
    'name':'stPID', 
    'position':(375, 145), 
    'text':'PID', 
    },

{'type':'Button', 
    'name':'btSelectLogToView', 
    'position':(336, 389), 
    'size':(21, 17), 
    'label':'...', 
    },

{'type':'TextField', 
    'name':'tfPID', 
    'position':(404, 142), 
    'size':(39, -1), 
    },

{'type':'TextField', 
    'name':'tfLogToProcess', 
    'position':(155, 386), 
    'size':(171, -1), 
    'text':'C:\\logs\\someprocess.hppie', 
    },

{'type':'StaticText', 
    'name':'StaticText10', 
    'position':(29, 393), 
    'text':'Heappie log to process', 
    },

{'type':'StaticText', 
    'name':'StaticText9', 
    'position':(403, 183), 
    'text':'Base address', 
    },

{'type':'TextField', 
    'name':'tfBaseAddress', 
    'position':(484, 177), 
    'size':(83, -1), 
    'text':'0x10000', 
    'toolTip':'Start offset', 
    },

{'type':'Button', 
    'name':'btSelectFileDump', 
    'position':(347, 180), 
    'size':(21, 17), 
    'label':'...', 
    },

{'type':'StaticText', 
    'name':'StaticText5', 
    'position':(27, 181), 
    'text':'Memory dump to import', 
    },

{'type':'CheckBox', 
    'name':'cbException', 
    'position':(407, 217), 
    'size':(168, -1), 
    'label':'Wait for an exception to start', 
    },

{'type':'Button', 
    'name':'btProcessList', 
    'position':(470, 142), 
    'label':'Select process', 
    },

{'type':'Button', 
    'name':'btSelectFileLog', 
    'position':(371, 214), 
    'size':(21, 17), 
    'label':'...', 
    },

{'type':'StaticBox', 
    'name':'StaticBox3', 
    'position':(10, 14), 
    'size':(574, 288), 
    'label':'Analysis Options', 
    },

{'type':'StaticBox', 
    'name':'StaticBox2', 
    'position':(10, 302), 
    'size':(574, 164), 
    'label':'Viewer Options', 
    },

{'type':'StaticBox', 
    'name':'StaticBox1', 
    'position':(10, 466), 
    'size':(469, 33), 
    },

{'type':'StaticText', 
    'name':'stStatus', 
    'position':(19, 478), 
    'foregroundColor':(80, 80, 80, 255), 
    'text':'Status...', 
    },

{'type':'TextField', 
    'name':'tfLogfile', 
    'position':(428, 353), 
    'size':(82, -1), 
    'text':'0xffffffff', 
    },

{'type':'TextField', 
    'name':'tfNumOfFiles', 
    'position':(155, 353), 
    'size':(73, -1), 
    'text':'0x0', 
    },

{'type':'Button', 
    'name':'btView', 
    'position':(254, 427), 
    'size':(79, -1), 
    'label':'View', 
    },

{'type':'StaticLine', 
    'name':'StaticLine1', 
    'position':(0, 0), 
    'size':(592, -1), 
    'layout':'horizontal', 
    },

{'type':'Button', 
    'name':'btStart', 
    'position':(254, 256), 
    'size':(79, -1), 
    'label':'Start analysis', 
    },

{'type':'TextField', 
    'name':'tfDumpFilePath', 
    'position':(153, 177), 
    'size':(183, -1), 
    'text':'C:\\memorydump.raw', 
    'toolTip':'Analyze a memory dump.', 
    },

{'type':'ComboBox', 
    'name':'cbPatternType', 
    'position':(425, 110), 
    'size':(68, -1), 
    'items':[u'ASCII', u'Hex'], 
    'stringSelection':'ASCII', 
    'text':'ASCII', 
    },

{'type':'StaticText', 
    'name':'StaticText6', 
    'position':(296, 360), 
    'text':'Max. address to process', 
    },

{'type':'StaticText', 
    'name':'StaticText4', 
    'position':(27, 360), 
    'text':'Min. address to process', 
    },

{'type':'StaticText', 
    'name':'StaticText3', 
    'position':(25, 217), 
    'text':'Log filename', 
    },

{'type':'TextField', 
    'name':'tfOutputPath', 
    'position':(153, 210), 
    'size':(209, -1), 
    'text':'C:\\logs\\someprocess.hppie', 
    'toolTip':'The log generated by Heappie!', 
    },

{'type':'TextField', 
    'name':'tfPattern', 
    'position':(153, 109), 
    'size':(256, -1), 
    'text':'AAAA', 
    'toolTip':'Pattern to track', 
    },

{'type':'StaticText', 
    'name':'StaticText2', 
    'position':(25, 115), 
    'text':'Pattern to match', 
    },

{'type':'TextField', 
    'name':'tfProcessName', 
    'position':(153, 142), 
    'size':(185, -1), 
    'editable':False, 
    'toolTip':'Press "Select process" to choose the process to attach', 
    },

{'type':'StaticText', 
    'name':'StaticText1', 
    'position':(25, 148), 
    'text':'Process to attach', 
    },

] # end components
} # end background
] # end backgrounds
} }
