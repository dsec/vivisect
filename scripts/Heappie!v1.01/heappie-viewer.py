# Copyright (c) 2012, CORE SECURITY TECHNOLOGIES
# All rights reserved.
#

import sys, os
import pygame
import optparse


class Bar:
    def __init__(self, surface, barNo):
        self.surface = surface
        self.barNo = barNo
        
    def draw_background(self):
        self.bar_width  = 1024
        self.bar_height = 100

        #draw the main rectangle ## pygame.Rect(left, top, width, height)
        self.left = 10 
        self.top = 100 + self.barNo * (10 + self.bar_height)

        container = pygame.Rect(self.left, self.top, self.bar_width, self.bar_height)
        pygame.draw.rect(self.surface, (100, 100, 100), container)
    
    def draw(self, chunks, color=(0, 0, 0xff)):
        self.chunks = chunks
        self.draw_background()
        self.color = color

        for chunk in chunks:
            #rect = pygame.Rect(chunk[0], 102 + self.barNo * 96, (chunk[1] - chunk[0]), 96)
            rect = pygame.Rect(chunk[0], 102 + self.barNo * (10 + self.bar_height), (chunk[1] - chunk[0]), 96)
            pygame.draw.rect(self.surface, color, rect)    
    
    def redraw(self):
        self.draw_background()
        
        for chunk in self.chunks:
            #rect = pygame.Rect(chunk[0], 102 + self.barNo * 96, (chunk[1] - chunk[0]), 96)
            rect = pygame.Rect(chunk[0], 102 + self.barNo * (10 + self.bar_height), (chunk[1] - chunk[0]), 96)
            pygame.draw.rect(self.surface, self.color, rect)    
            
    def highlight(self, chunk_num, color=(0xff, 0, 0)):
        chunk = self.chunks[chunk_num]
 
        self.redraw()

        #rect = pygame.Rect(chunk[0], 102 + self.barNo * 96, (chunk[1] - chunk[0]), 96)
        rect = pygame.Rect(chunk[0], 102 + self.barNo * (10 + self.bar_height), (chunk[1] - chunk[0]), 96)
        pygame.draw.rect(self.surface, color, rect, 2)    
    

class Offset_bar:
    def __init__(self, chunks):
        self.chunks = chunks

class Intersector:
    def intersect(self, bars):
        result = bars[0].chunks
        for b in bars[1:]:
            result = self.intersect2(b.chunks, result)
            
        return result

    def intersect2(self, a, b):
        result = []
        for c in a:
            for c2 in b:
                i1, i2 = min(c[1], c2[1]), max(c[0], c2[0])
                
                if i1 > i2:
                    result.append((i2, i1))
                    
        return result
                
            
        

def draw_chunks(window, chunks, color=(0, 0, 0xff)):
    for chunk in chunks:
        rect = pygame.Rect(chunk[0], 102, (chunk[1] - chunk[0]), 96)
        pygame.draw.rect(window, color, rect)    


def get_ratio(resolution, min, max, size):
    return resolution * 1.0 / ((max + size) - min)


		
def pos_to_address(ratio, pos, minaddr):
    return minaddr + ((pos-10) / ratio  )


def get_main_ratio(resolution, chunks_for_bar):
    # Searche for highest and lowest addresses, then returns the corresponding ratio
    
    max = 0
    size = []
    
    
    for chunks_in_bar in chunks_for_bar:
        if max < (chunks_in_bar[-1][0] + chunks_in_bar[-1][1]):
            max = chunks_in_bar[-1][0]
            size = chunks_in_bar[-1][1]


    min = 0xffffffff
    for chunks_in_bar in chunks_for_bar:
        if min > chunks_in_bar[0][0]:
            min = chunks_in_bar[0][0]

    ratio = get_ratio(resolution, min, max, size)

    return ratio, min


        
def normalize(abs_min, chunks, ratio=0):
    minaddr = abs_min #chunks[0][0]
    size    = chunks[-1][1]

    return [ ((real_offset-minaddr) * ratio +11 , ((real_offset-minaddr) + size ) * ratio+11  ) for real_offset , size in chunks  ]

	
def parse_logfiles(logfiles, minaddr, maxaddr):


    # ##############################################
    # Parse logfile
    # ##############################################

    chunks_for_bar = []

    for logfilename in logfiles:
       
        # Check for empty files
        if os.stat(logfilename)[6]==0:
            print "[-] Logfile %s is empty" % logfilename
            exit(0)
       
        logfile = open(logfilename, "r" )
        lists_of_chunks = []
        address_to_pos = {}
        
        

        for line in logfile.readlines():
            try:
                if minaddr < int(line[8:18],16) < maxaddr:
                    lists_of_chunks.append((int(line[8:18],16), int(line[20:30],16)) )

            except Exception, e:
                pass
        
        chunks_for_bar.append(lists_of_chunks)
    
    return chunks_for_bar


def identify_chunk(pos, bar_list, map_pixel_to_offset, has_intersect):



    highlight_data = {}

    # Identify Bar
    bar_num = 0
    for bar in bar_list:
        if bar.top < pos[1] < (bar.top + bar.bar_height):
            break
        bar_num += 1
    

    if bar_num+1 > len(bar_list):
        return 0,0, False


    pos_to_addr = {}


    # Limit focus to last not intersected bar
    if not has_intersect:
        if bar_num == len(bar_list)-1:
            bar_num -= 1

    highlight_data["bar_num"] = bar_num

    # Identify Chunk
    highlight_data["chunk_num"] = 0

    addr = 0
    size = 0
   
    for pixel_data, offset_data in map_pixel_to_offset[bar_num]:
        if pixel_data[0] < pos[0] < pixel_data[1]:      
            return offset_data[0], (offset_data[1]-offset_data[0]), highlight_data
        highlight_data["chunk_num"] += 1



    return 0,0,False

def reset_highlight(bar_objects, highlight_data):
    
    
    if highlight_data == False:
        for bar in bar_objects:
            bar.redraw()
        return

    if highlight_data.has_key("intersect_bar"):
        highlight_data["intersect_bar"].highlight(highlight_data["chunk_num"],color=(0xff, 0, 0))
    else:
        bar_objects[highlight_data["bar_num"]].highlight(highlight_data["chunk_num"])
    
    

	
def main():

    # Beautiful banner
    banner = r"""

.---.  .---.     .-''-.     ____    .-------. .-------. .-./`)     .-''-.   
|   |  |_ _|   .'_ _   \  .'  __ `. \  _(`)_ \\  _(`)_ \\ .-.')  .'_ _   \  
|   |  ( ' )  / ( ` )   '/   '  \  \| (_ o._)|| (_ o._)|/ `-' \ / ( ` )   ' 
|   '-(_{;}_). (_ o _)  ||___|  /  ||  (_,_) /|  (_,_) / `-'`"`. (_ o _)  | 
|      (_,_) |  (_,_)___|   _.-`   ||   '-.-' |   '-.-'  .---. |  (_,_)___| 
| _ _--.   | '  \   .---..'   _    ||   |     |   |      |   | '  \   .---. 
|( ' ) |   |  \  `-'    /|  _( )_  ||   |     |   |      |   |  \  `-'    / 
(_{;}_)|   |   \       / \ (_ o _) //   )     /   )      |   |   \       /  
'(_,_) '---'    `'-..-'   '.(_,_).' `---'     `---'      '---'    `'-..-'   

::::::::::::::::::::::..:::::..::::::::: Heap Spray Viewer 1.0b ::::::::::
::::::::::::::::::::::..:::::..::::::::: asacco@coresecurity.com (aLS) :::
                    
    """
    print banner
    
    # ##############################################
    # Arguments parsing
    # ##############################################

    parser = optparse.OptionParser("%prog [options] {logfile1, logfile2, logfile3}")

    parser.add_option("-m", "--min", dest="minaddr", 
                    default="0",type="int", 
                    help="The lower address to include in the graphic")
    parser.add_option("-M", "--max", dest="maxaddr", 
                    default="0xffffffff",type="int", 
                    help="The higher address to include in the graphic")

    

    (options, args) = parser.parse_args()

    if len(args) < 1:
        parser.error("Incorrect arguments. Use --help for help")


    logfilename = args[0]
    
    minaddr = options.minaddr
    maxaddr = options.maxaddr
    has_intersect = True


    # ##############################################
    # Parse logfile
    # ##############################################

    chunks_for_bar = parse_logfiles(args, minaddr, maxaddr)


    # ##############################################
    # Pygame Initialization
    # ##############################################

    # Resolution    
    container_width  = 1024
    container_height = 200 + (200*len(args))

    pygame.init() 

    # Create the screen
    window = pygame.display.set_mode((container_width+20, container_height)) 
    window.set_alpha(0x50)
    

    # Draw the main rectangle
    container = pygame.Rect(10, 100, container_width, 100)

    # Set a surface to place the text
    background = pygame.Surface((800,50))
    background = background.convert()
    background.fill((0, 0, 0))

    # Place the text in it
    font = pygame.font.Font(None, 36)
    text = font.render("Address: ", 1, (255, 255, 255))
    text_block = font.render("Block info:", 1, (100, 255, 255))

    textpos = text.get_rect(centerx=background.get_width()/2)
    blockpos = text.get_rect(centerx=background.get_width()/2, centery=35)

    background.blit(text, textpos)
    background.blit(text_block, blockpos)
 
    # Get ratio
    ratio, abs_min = get_main_ratio(container_width, chunks_for_bar)


    #####################################################
    # Map pixels to offset                              #
    #####################################################
    
    pixels = []
    for list_of_chunks in chunks_for_bar:
        pixels.append(normalize(abs_min, list_of_chunks, ratio))
        
    offsets = []
    for chunks_for_one_bar in chunks_for_bar:
        temp = []
        for (x, y) in chunks_for_one_bar:
            t = (x, x+y)
            temp.append(t)
        offsets.append(temp)
    
    assert(len(pixels) == len(offsets))
    
    map_pixel_to_offset = []
    for i in range(len(pixels)):
        map_pixel_to_offset.append(zip(pixels[i], offsets[i]))


    #####################################################
    # Map pixels to offset (intersect bar)              #
    #####################################################
    
    
    # Get intersection with offsets
    offsets_bar = []
    temp = []

    for chunks_for_one_bar in chunks_for_bar:
        for offset, size in chunks_for_one_bar:
            temp.append((offset, offset+size))
        offsets_bar.append(Offset_bar(temp))
        temp = []
    
    intersected_offsets_size = Intersector().intersect(offsets_bar)
    
    # If there is at least one intersection
    if intersected_offsets_size: 
        
        # Get intersection with pixels
        pixels_inter_offsets = []
       
        for x, y in intersected_offsets_size:
            pixels_inter_offsets.append((x, y-x))

        pixels_inter_offsets = normalize(abs_min, pixels_inter_offsets, ratio)
        
        # Map pixel to offset (intersect)
        inter_map_pixel_to_offset = []

        for i in range(len(pixels_inter_offsets)):
            inter_map_pixel_to_offset.append((pixels_inter_offsets[i], intersected_offsets_size[i]))
            
        # Add the new mapping to the list of mappings
        map_pixel_to_offset.append(inter_map_pixel_to_offset)

    else:
        has_intersect = False
        print "[-] There is no intersection"

    # FIN HACK
    #####################################################



    # Process data per sample
    index = 0

    bar_objects = []    
    for list_of_chunks in chunks_for_bar:
        print "[+] Drawing memory map %d" % (index+1)

        # Normalize data
        chunks = normalize(abs_min, list_of_chunks, ratio)

        # Draw chunks
        bar = Bar(window, index)
        bar.draw(chunks)
        # debug

        bar_objects.append(bar)
        index += 1
        
    # Calculate the intersection only if there is more than one sample
    if len(args) > 1:
        print "[+] Drawing intersection map"
        interchunks = Intersector().intersect(bar_objects)
        intersection_bar = Bar(window, len(args)+1)
        intersection_bar.draw(interchunks, color=(255, 255, 51))


        # Add the intersection bar to bar_objects
        bar_objects.append(intersection_bar)

    

    #draw it to the screen
    pygame.display.flip() 

    # Get the lower address of the list of chunks
    min_addr = chunks_for_bar[0][0][0]
    
    # Input handling (somewhat boilerplate code):
    window.blit(background, (0, 0))
    pygame.display.flip()


    # Handle events
    while True: 
        for event in pygame.event.get(): 
            if event.type == pygame.QUIT: 
                sys.exit(0) 

            elif event.type == pygame.MOUSEMOTION:
                if (100 < event.pos[1] < 800):

                    addr_num = pos_to_address(ratio, event.pos[0], min_addr)
                    chunk_addr, chunk_size, highlight_data = identify_chunk(event.pos, bar_objects, map_pixel_to_offset, has_intersect)

                    reset_highlight(bar_objects, highlight_data)
                    addr = "Address: 0x%08x" % addr_num
                    block = "Block at: 0x%08x, Size: %x" % (chunk_addr, chunk_size)

                    text = font.render(addr, 1, (255, 255, 255))
                    text_block = font.render(block, 1, (100, 255, 255))
                    background.fill( (0,0,0,0) )
                    background.blit(text, textpos)
                    background.blit(text_block, blockpos)
                    window.blit(background, (0, 0))

                    pygame.display.flip()



if __name__ == "__main__":
    sys.exit(main())
