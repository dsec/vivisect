# Copyright (c) 2012, CORE SECURITY TECHNOLOGIES
# All rights reserved.
#!/usr/bin/python

"""
__version__ = "$Revision: 1.00 $"
__date__ = "$Date: 2012/01/01 22:13:31 $"
"""

from PythonCard import model, dialog, timer
from gui import process_list_window, multiple_logs
import ConfigParser, os, sys, popen2
import thread, time


class RedirectText(object):
    def __init__(self,aWxTextCtrl):
        self.out=aWxTextCtrl
 
    def write(self,string):
        self.out.appendText(string)
        
        
class HeappieGUI(model.Background):

    def on_initialize(self, event):

        # Redirect text to console
        redir=RedirectText(self.components.txtConsole)
        sys.stdout=redir        
        sys.stderr=redir        
        
        # Initialize components
        self.stop_run = False
        self.update_conf()
        self.type_of_analysis = None
        self.components.SelectAnalysis.editable = "False"
        self.components.txtConsole.foregroundColor = (1,255, 0)# Super nerd green ### (214, 214, 214) # Light gray #### 
        self.is_attached = False
        
    def openFile(self, wildcard = "Config files (*.cfg)|*.cfg;*.CFG"):
        self.openingFileDialog = 1
        directory = self.application.applicationDirectory

        result = dialog.openFileDialog(None, "Import which file?", directory, '', wildcard)
        if result.accepted:
            path = result.paths[0]
            self.orig_file = path
            return path

    def shellquote(s):
        return "'" + s.replace("'", "'\\''") + "'"
            
    def update_conf(self):
        self.pid = self.components.tfPID.text
        self.pattern = self.components.tfPattern.text
        self.pattern_type = self.components.cbPatternType.text
        self.output_path = self.components.tfOutputPath.text
        self.wait_exception = self.components.cbException.checked
        self.process_name = self.components.tfProcessName.text

        self.min_addr = self.components.tfNumOfFiles.text
        self.max_addr = self.components.tfLogfile.text
        self.log_to_process = self.components.tfLogToProcess.text
        self.base_address = self.components.tfBaseAddress.text
    
    def update_gui(self):
        self.components.tfPID.text = str(self.pid)
        self.components.tfPattern.text = self.pattern
        self.components.cbPatternType.text = self.pattern_type
        self.components.tfOutputPath.text = self.output_path
        self.components.cbException.checked = self.wait_exception
        self.components.tfProcessName.text = self.process_name
        self.components.tfBaseAddress.text = self.base_address

        self.components.tfNumOfFiles.text = self.min_addr
        self.components.tfLogfile.text = self.max_addr
        self.components.tfLogToProcess.text = self.log_to_process
        
    def set_analysis_attach(self):

        self.is_attached = True
        self.components.tfDumpFilePath.enabled = False
        self.components.tfBaseAddress.enabled = False

        self.components.btProcessList.enabled = True
        self.components.tfProcessName.enabled = True
        self.components.tfPattern.enabled = True
        self.components.tfOutputPath.enabled = True
        self.components.tfPID.enabled = True

    
    def set_analysis_dump(self):

        self.is_attached = False
        self.components.tfDumpFilePath.enabled = True
        self.components.tfBaseAddress.enabled = True
        self.components.tfOutputPath.enabled = True
        
        self.components.btProcessList.enabled = False
        self.components.tfProcessName.enabled = False
        self.components.tfPID.enabled = False




# ############################################################################
# Buttons
# ############################################################################


    def on_btProcessList_mouseClick(self, event):
        # Call to the process list function and get the PID, procname that it returns
        process_info = process_list_window.process_list_window(self)
        self.update_conf()
        if process_info:
            self.pid = int(process_info[0])
            self.process_name = process_info[1]
            self.components.tfProcessName.enabled = False

            self.update_gui()
      
    def on_btSelectFileLog_mouseClick(self, event):
        self.openingFileDialog = 1
        path = self.openFile(wildcard = "All files (*.*)|*.*;*.*")
        self.output_path = self.components.tfOutputPath.text = path

    def on_btSelectFileDump_mouseClick(self, event):
        self.openingFileDialog = 1
        path = self.openFile(wildcard = "All files (*.*)|*.*;*.*")

        if path:
            self.components.tfProcessName.enabled = False

        self.components.tfDumpFilePath.enabled = True
        self.dump_path = self.components.tfDumpFilePath.text = path

    def on_btReport_mouseClick(self, event):
        #self.stop_run = True
        plain_text_log = self.logfilename + ".txt"
        os.system("python crash_logger_report.py -v %s > %s" % (self.logfilename, plain_text_log))
        os.system(plain_text_log)

    def on_btStop_mouseClick(self, event):
        self.stop_run = True

    def on_btSelectLogToView_mouseClick(self, event):
        self.openingFileDialog = 1
        path = self.openFile(wildcard = "All files (*.*)|*.*;*.*")
        self.log_to_process = self.components.tfLogToProcess.text = '"'+ path + '"'

    def on_SelectAnalysis_select(self, event):
        if "Attach" in self.components.SelectAnalysis.stringSelection:
            self.set_analysis_attach()
        elif "dump":
            self.set_analysis_dump()
        else:
            self.components.stStatus.Text = "[-] You must select a type of analysis."
            print "[-] You must select a type of analysis."

    def on_btnConsole_mouseClick(self, event):
        if self.components.btnConsole.checked:
            self.application.backgrounds[0].Size = (602, 741)
            self.application.backgrounds[0].Refresh()
        else:
            self.application.backgrounds[0].Size = (602, 551)
            self.application.backgrounds[0].Refresh()
            
    def on_btIntersect_mouseClick(self, event):
        # Call to the intersect_multiple_logs function and update the LOG field with the selected logs
        logs_list = multiple_logs.multiple_logs(self)
        print repr(logs_list)
        self.log_to_process = "".join(['"'+a+'" ' for a in logs_list])
        print repr(self.log_to_process)
        self.update_gui()


    def on_btView_mouseClick(self, event):
        self.stop_run = False
        self.update_conf()
        thread.start_new_thread(self.draw_analysis, ())


    def on_btStart_mouseClick(self, event):
        self.stop_run = False
        self.update_conf()
        thread.start_new_thread(self.start_analysis, ())




# ############################################################################
# Actions
# ############################################################################

    def start_analysis(self):
        # Process the options
        self.update_conf()

        # Set the new log as the log to view 
        self.log_to_process = self.components.tfLogToProcess.text = self.output_path = self.components.tfOutputPath.text
        self.components.tfLogToProcess.text = '"' + self.log_to_process + '"'

        # Type of pattern to search
        if self.pattern_type == "Hex":
            type = "-x"
        else:
            type = "" 

        # Start analysis only after an exception
        if self.wait_exception:
            waitexcept = "-e"
        else:
            waitexcept = ""

        if self.is_attached:
            #Attach to process
            self.components.stStatus.text = "Process analysis started"
            try:
                cmd = 'python heappie-analyzer.py -p' + self.pid + ' ' + self.pattern + ' -l "' + self.output_path + '" ' + type + ' ' + waitexcept
                self.launch(cmd)
            except:
                pass

        else: 
            # Analyze memory dump
            self.components.stStatus.text = "Analyzing memory dump."
            try:
                cmd = 'python heappie-analyzer.py -d "' + self.dump_path + '" -b' + self.base_address + ' -l "' + self.output_path + '" ' + self.pattern
                self.launch(cmd)
            except:
                pass
            
        self.components.stStatus.text = "Analysis Done."

            
    def draw_analysis(self):
        
        # Process the options
        self.update_conf()

        try:
            cmd = 'python heappie-viewer.py' + ' -m ' + self.min_addr + ' -M' + self.max_addr + ' ' + self.log_to_process
            self.launch(cmd)
            
        except Exception, e:
            print e
            pass
        
        self.components.stStatus.text = "Analysis Done."

        
    def launch(self, cmd):
        print "Command:", cmd
        stdout, stdin = popen2.popen4(cmd)
        for line in stdout:
            self.components.txtConsole.appendText(line)

    
if __name__ == '__main__':
    app = model.Application(HeappieGUI)
    app.MainLoop()
