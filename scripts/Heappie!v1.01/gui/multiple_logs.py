
"""
__version__ = "$Revision: 1.3 $"
__date__ = "$Date: 2004/08/12 19:14:23 $"
"""

from PythonCard import model, dialog
import vtrace

class Multiple_logs(model.CustomDialog):
    def __init__(self, parent, job = None):
        model.CustomDialog.__init__(self, parent)
        self.save = False
        self.initialize_list()
        self.selected_process = ""
        self.logs = []

#    def on_selectSample_gainFocus(self, event):
#        if self.components.selectSample.text in ["<select sample>", ""]:
#            self.components.selectSample.text = ""
#            self.components.selectSample.foregroundColor = 'black'
            
#    def on_selectSample_loseFocus(self, event):
#        if self.components.selectSample.text in ["<select sample>", ""]:
#            self.components.selectSample.text = "<select sample>"
#            self.components.selectSample.foregroundColor = 'grey'
            


    def initialize_list(self):

        #for item in processes:
        #    items_list.append((str(item[0]), item[1]))

        #self.components.logList.items = items_list
        pass

    def sliceStr(self, slices):
        return repr(slices)

        
    def on_addLogBtn_mouseClick(self, event):
        path = self.openFile("All files (*.*)|*.*;*.*")

        if path:
            self.add_loglist_item(path)

            
    def on_acceptBtn_mouseClick(self, event):
        
        for path in self.components.logList.items:
            self.logs.append(path)
        
        self.destroy()


    def on_cancelBtn_mouseClick(self, event):
        self.save = False
        self.destroy()


    def load_logList(self):
        self.components.logList.Clear()

        for log in self.logs:
            self.add_loglist_item(log)


    def add_loglist_item(self, log):
        self.components.logList.Append([log])


    def openFile(self, wildcard):
        self.openingFileDialog = 1
        directory = self.application.applicationDirectory

        result = dialog.openFileDialog(None, "Import which file?", directory, '', wildcard)
        if result.accepted:
            path = result.paths[0]
            self.orig_file = path
            return path 



#def myDialog(parent, txt):
def multiple_logs(parent, job = None):
    dlg = Multiple_logs(parent, job)
    dlg.showModal()
    job = {}

    dlg.destroy()
    return dlg.logs
