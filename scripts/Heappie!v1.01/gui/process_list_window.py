
"""
__version__ = "$Revision: 1.3 $"
__date__ = "$Date: 2004/08/12 19:14:23 $"
"""

from PythonCard import model, dialog
import vtrace

class Process_list_window(model.CustomDialog):
    def __init__(self, parent, job = None):
        model.CustomDialog.__init__(self, parent)
        self.save = False
        self.initialize_list()
        self.selected_process = ""
        self.components.processList.SetColumnWidth(0, 50)


    def initialize_list(self):

        dbg = vtrace.getTrace()
        processes = dbg.ps()
        
        items_list = []

        for item in processes:
            items_list.append((str(item[0]), item[1]))

        self.components.processList.items = items_list
        dbg.release()

    def sliceStr(self, slices):
        return repr(slices)

    def on_acceptBtn_mouseClick(self, event):
        self.selected_process = self.components.processList.items[self.components.processList.GetFocusedItem()]
        self.destroy()

    def on_processList_mouseDoubleClick(self, event):
        self.selected_process = self.components.processList.items[self.components.processList.GetFocusedItem()]
        self.destroy()
        
    def on_cancelBtn_mouseClick(self, event):
        self.save = False
        self.destroy()

#def myDialog(parent, txt):
def process_list_window(parent, job = None):
    dlg = Process_list_window(parent, job)
    dlg.showModal()
    job = {}

    dlg.destroy()
    return dlg.selected_process
